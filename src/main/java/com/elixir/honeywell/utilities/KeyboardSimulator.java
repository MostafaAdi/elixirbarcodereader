/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.elixir.honeywell.utilities;

import java.awt.AWTException;
import java.awt.HeadlessException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;

/**
 *
 * @author Administrator
 */
public class KeyboardSimulator extends Robot{
    public KeyboardSimulator() throws AWTException {

    }

    public void pasteToUser(String text) {
        StringSelection selection = new StringSelection(text);
        while (true) {
            try {
                Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
                clipboard.setContents(selection, null);
                keyPress(KeyEvent.VK_CONTROL);
                keyPress(KeyEvent.VK_V);
                keyRelease(KeyEvent.VK_CONTROL);
                keyRelease(KeyEvent.VK_V);
                return;
            } catch (HeadlessException e) {
            }
            try {
                Thread.sleep(40);
            } catch (InterruptedException e) {

            }
        }
    }

    public void pasteToUser() {
        keyPress(KeyEvent.VK_CONTROL);
        keyPress(KeyEvent.VK_V);
        keyRelease(KeyEvent.VK_CONTROL);
        keyRelease(KeyEvent.VK_V);
    }

    public void copyToUser(String text) {
        StringSelection selection = new StringSelection(text);
        while (true) {
            try {
                Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
                clipboard.setContents(selection, null);
                return;
            } catch (HeadlessException e) {
            }
            try {
                Thread.sleep(40);
            } catch (InterruptedException e) {

            }
        }
    }

    public void keyPressWithRelease(int key, int times) {
        for (int i = 0; i < times; i++) {
            keyPress(key);
            keyRelease(key);
        }
    }

    public void keyPressWithRelease(int key) {
        keyPress(key);
        keyRelease(key);
    }

    public void keyPressWithReleaseAndDelay(int key, int delay) {
        keyPress(key);
        delay(delay);
        keyRelease(key);
        delay(delay);
    }

    public void keyPressWithReleaseAndDelay(int key, int times, int delay) {
        for (int i = 0; i < times; i++) {
            keyPress(key);
            delay(delay);
            keyRelease(key);
            delay(delay);
        }
    }
}
