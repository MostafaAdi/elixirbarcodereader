/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.elixir.honeywell.utilities;

import bsh.EvalError;
import bsh.Interpreter;
import java.awt.event.KeyEvent;

/**
 *
 * @author Administrator
 */
public class DataRobot {

    public final KeyboardSimulator robot;
    private Interpreter interpreter;
    private final String evalCode;

    public DataRobot(String fileText, KeyboardSimulator robot) {
        this.robot = robot;
        evalCode = fileText;
    }

    public boolean execute(String data) {
        System.out.println("executing keyboard command.....");
        interpreter = new Interpreter();
        robot.keyPress(KeyEvent.VK_CONTROL);
        robot.keyPress(KeyEvent.VK_A);
        robot.keyRelease(KeyEvent.VK_CONTROL);
        robot.keyRelease(KeyEvent.VK_A);
        try {
            interpreter.set("dataText", data);
            interpreter.set("EUtility", this);
            interpreter.eval(evalCode);
            return (boolean) interpreter.get("executed");
        } catch (EvalError e) {
        }
        return false;
    }

    public void pasteData(String data, int tabs) {
        robot.pasteToUser(data);
        robot.keyPressWithRelease(KeyEvent.VK_TAB, tabs);
    }

    public boolean containsNumber(String text) {
        return text.replaceAll("[0-9]", "").length() < text.length();
    }

    public boolean containsArabic(String text) {
        return text.replaceAll("[\\u0600-\\u06FF]", "").length() < text.length();
    }

    public boolean containsEnglish(String text) {
        return text.replaceAll("[a-zA-Z]", "").length() < text.length();
    }

    public String removeAllArabic(String text) {
        return text.replaceAll("[\\u0600-\\u06FF]", "");
    }

    public String removeAllButArabic(String text) {
        return text.replaceAll("[^\\u0600-\\u06FF\\s]", "");
    }

    public String removeAllEnglish(String text) {
        return text.replaceAll("[a-zA-Z]", "");
    }

    public String removeAllButEnglish(String text) {
        return text.replaceAll("[^a-zA-Z]", "");
    }

    public String removeAllNumbers(String text) {
        return text.replaceAll("[0-9]", "");
    }

    public String removeAllButNumbers(String text) {
        return text.replaceAll("[^0-9]", "");
    }

}

