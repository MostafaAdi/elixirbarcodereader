/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.elixir.honeywell.utilities;

import com.elixir.honeywell.MainForm;
import com.fazecast.jSerialComm.SerialPort;
import com.fazecast.jSerialComm.SerialPortDataListener;
import com.fazecast.jSerialComm.SerialPortEvent;
import com.tulskiy.keymaster.common.HotKey;
import com.tulskiy.keymaster.common.Provider;
import java.awt.AWTException;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.TooManyListenersException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.imageio.ImageReadParam;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import javax.swing.KeyStroke;
import javax.xml.bind.DatatypeConverter;

/**
 *
 * @author Administrator
 */



public class ScanHandler {

    private final Provider provider = Provider.getCurrentProvider(false);
//    private final MainForm mainForm;
    private final List<String> shortcuts;
    //Serial Port
    private final SerialPort serialPort;
    private final InputStream serialInputStream;
    private final OutputStream serialOutputStream;
    //List used to get the data from the InputStream
    private final List<Byte> bytesList = new ArrayList<>();

    public List<DataRobot> dataRobots;
    private KeyboardSimulator keyboardSimulator;
    private boolean captureMode = false;
//    private boolean barcodeMode = false;
    private int imageNumber = -1;
    private boolean foundDevice = false;

    public ScanHandler(MainForm mainForm, String port)
    throws IOException, TooManyListenersException, AWTException {
        System.out.println("initializing scanner....");
        serialPort = SerialPort.getCommPorts()[0];
        this.foundDevice = serialPort.openPort();
        serialPort.addDataListener(new SerialPortDataListener() {
            @Override
            public int getListeningEvents() { 
                return SerialPort.LISTENING_EVENT_DATA_AVAILABLE; 
            }
            @Override
            public void serialEvent(SerialPortEvent event) {
                    
                if (event.getEventType() != SerialPort.LISTENING_EVENT_DATA_AVAILABLE)
                    return;
                if (captureMode) {
                    handleImage();
                    return;
                }
                handleBarcode();
                
            }
        });

        serialOutputStream = serialPort.getOutputStream();
        serialInputStream = serialPort.getInputStream();
        
        this.dataRobots = new ArrayList<>();
        shortcuts = new ArrayList<>();
        for (int i = 1; i < 10; i++) {
            shortcuts.add("control shift F" + i);
        }
        
        this.loadSettings();
        this.initListners();

    }
    
    public ScanHandler(String port)
    throws IOException, TooManyListenersException, AWTException {
        
        serialPort = SerialPort.getCommPort(port);
        this.foundDevice = serialPort.openPort();
        if(this.foundDevice){
            System.out.println("device found.");
        }else{
            System.out.println("ERROR: no device found.");
        }
        serialPort.addDataListener(new SerialPortDataListener() {
            @Override
            public int getListeningEvents() { 
                return SerialPort.LISTENING_EVENT_DATA_AVAILABLE; 
            }
            @Override
            public void serialEvent(SerialPortEvent event) {
                    
                if (event.getEventType() != SerialPort.LISTENING_EVENT_DATA_AVAILABLE)
                    return;
                if (captureMode) {
                    handleImage();
                    return;
                }
                handleBarcode();
                
            }
        });

        serialOutputStream = serialPort.getOutputStream();
        serialInputStream = serialPort.getInputStream();
        
        this.dataRobots = new ArrayList<>();
        shortcuts = new ArrayList<>();
        for (int i = 1; i < 10; i++) {
            shortcuts.add("control shift F" + i);
        }
        
        this.loadSettings();
        this.initListners();
    }

    private byte[] hexToBytes(String hex) {
        return DatatypeConverter.parseHexBinary(hex);
    }

    private void writeToOutputStream(byte[] data) {
        try {
            serialOutputStream.write(data);
        } catch (IOException e) {
        }
    }

    private void capture() {
        if (captureMode) return;
        captureMode = true;
        /*
        *each command must be prefixed with
        * 1- unprintable ascii character (syn),
        * 2- ascii code for the letter M
        * 3- ascii code for newline
        *when you turn ascii code of characters above to hex code you get: 164d0d
        */
        
        //turn off in-stand sensor mode if active because it prevents device from taking images.
        //read documentation for further info (search for TRGSSW0).
        writeToOutputStream(hexToBytes("164d0d545247535357302e"));
        //imaging command from documentation converted from string to hex representation.
        //read documentation for further info (search for IMGSNP).
        writeToOutputStream(hexToBytes("164d0d494d47534e5031423b494d47534850304c3054383433523633394235304a36462e"));
    }

    private void handleImage() {
        try {
            /**
             * The Honeywell sends the image as parts so we save the
             * parts until we reached [46,6,70] it means that we reached
             * the end of the photo so we call The save function
             *
             */
            
            byte[] currentByteArray = new byte[serialInputStream.available()];
            serialInputStream.read(currentByteArray);
            for (byte currentByte : currentByteArray) {
                bytesList.add(currentByte);
            }
            System.out.println("reading from input stream: "+bytesList);
            
            if (checkEndOfImage())
                saveCapture(); 
           
        } catch (IOException | InterruptedException ex) {
            Logger.getLogger(ScanHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void handleBarcode() {

        try {
            /**
             * This is the bar-code reading mode , we just put the data
             * into a map and then send it to the data robot that has
             * the meta for this data
             */
            BufferedReader br = new BufferedReader(new InputStreamReader(serialInputStream, "Windows-1256"));
            StringBuilder currentStringBuilder = new StringBuilder();
            while (br.ready()) {
                currentStringBuilder.append((char) br.read());
            }
            br.close();
            
            if (checkPassportOrId(currentStringBuilder.toString())) {
                for (DataRobot robot : dataRobots) {
                    if (robot.execute(currentStringBuilder.toString())) {
                        break;
                    }
                }
                return;
            }    keyboardSimulator.pasteToUser(currentStringBuilder + ",");
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(ScanHandler.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ScanHandler.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }
    
    private boolean checkPassportOrId(String data) {
        long count = data.chars().filter(ch -> ch == '#').count();
        // id normally contains 12 hashtags 
        //this is a work-around to diffrentiate id barcodes from packages barcodes
        return count > 8;
    }
    
    private void saveCapture() throws IOException, UnsupportedEncodingException, InterruptedException {
        
        captureMode = false;
        int imageStartIndex = 0;
        byte[] byteArray = new byte[bytesList.size()];
        boolean found = false;
        int i = 0;
        //Converting the BytesList into a byte Array
        for (byte listByte : bytesList) {
            byteArray[i] = listByte;
            i++;
        }
        
        //Searching for the start of the image 
        if (byteArray[39] == hexToBytes("ff")[0] && byteArray[40] == hexToBytes("d8")[0]) {
            imageStartIndex = 39;
            found = true;
        } else {
            byte previousByte = byteArray[0];
            for (byte currentByte : byteArray) {
                if (previousByte == hexToBytes("ff")[0] && currentByte == hexToBytes("d8")[0]) {
                    found = true;
                    imageStartIndex--;
                    break;
                }
                previousByte = currentByte;
                imageStartIndex++;
            }
        }
        if (!found) {
            System.out.println("Error");
            bytesList.clear();
            return;
        }

        ByteArrayInputStream bis = new ByteArrayInputStream(byteArray, imageStartIndex, byteArray.length - imageStartIndex);
        Iterator<?> readers = ImageIO.getImageReadersByFormatName("jpg");

        //ImageIO is a class containing static methods for locating ImageReaders
        //and ImageWriters, and performing simple encoding and decoding. 
        ImageReader reader = (ImageReader) readers.next();
        Object source = bis;
        ImageInputStream iis = ImageIO.createImageInputStream(source);
        reader.setInput(iis, true);
        ImageReadParam param = reader.getDefaultReadParam();

        Image image = reader.read(0, param);
        //got an image file

        BufferedImage bufferedImage = new BufferedImage(image.getWidth(null), image.getHeight(null), BufferedImage.TYPE_INT_RGB);
        //bufferedImage is the RenderedImage to be written

        Graphics2D g2 = bufferedImage.createGraphics();
        g2.drawImage(image, null, null);
        new File("captures").mkdir();
        File imageFile = new File("captures/Capture.jpg");
        ImageIO.write(bufferedImage, "jpg", imageFile);
        bytesList.clear();
        if (imageNumber != -1) {
            keyboardSimulator.copyToUser(imageFile.getAbsolutePath());

            keyboardSimulator.keyPress(KeyEvent.VK_CONTROL);
            keyboardSimulator.keyPress(KeyEvent.VK_SHIFT);
            keyboardSimulator.keyPress(0x30 + imageNumber);

            keyboardSimulator.keyRelease(KeyEvent.VK_CONTROL);
            keyboardSimulator.keyRelease(KeyEvent.VK_SHIFT);
            keyboardSimulator.keyRelease(0x30 + imageNumber);

            Thread.sleep(1200);
            keyboardSimulator.pasteToUser(imageFile.getAbsolutePath());
            Thread.sleep(40);
            keyboardSimulator.keyPressWithRelease(KeyEvent.VK_ENTER);
        }
        imageNumber = -1;
    }
    
    private boolean checkEndOfImage () {
        return bytesList.get(bytesList.size() - 1) == 46
            && bytesList.get(bytesList.size() - 2) == 6
            && bytesList.get(bytesList.size() - 3) == 70;
    }
    
    private void initListners() {
        System.out.println("registering keyboard listeners.....");
        for (String shortcut: shortcuts) {
            System.out.println(shortcut);
        }
        for (String shortcut : shortcuts) {
            provider.register(KeyStroke.getKeyStroke(shortcut), (HotKey hotkey) -> {
                imageNumber = Integer.parseInt(String.valueOf(shortcut.charAt(shortcut.length() - 1)));
                capture();
            });
        }

//        provider.register(KeyStroke.getKeyStroke(switchSortcut), (HotKey hotkey) -> {
//            if (!captureMode) {
//                barcodeMode = !barcodeMode;
//            }
//            mainForm.updateMode(barcodeMode);
//            System.out.println("captureMode: " + captureMode);
//            System.out.println("barcodeMode: " + barcodeMode);
//        });

    }

    private void loadSettings() throws AWTException, FileNotFoundException, IOException {
        keyboardSimulator = new KeyboardSimulator();
        keyboardSimulator.setAutoDelay(15);
        
        String currentPath = getCurrentPath();
        System.out.println(currentPath);
        File dir = new File("meta");
        File[] metaDirectoryFiles = dir.listFiles();
        System.out.println("before loop");
        if (metaDirectoryFiles != null) {
            for (File file : metaDirectoryFiles) {
                System.out.println("reading meta .....");
                FileInputStream is = new FileInputStream(file);
                InputStreamReader isr = new InputStreamReader(is, "UTF-8");
                BufferedReader br = new BufferedReader(isr);
                StringBuilder formula = new StringBuilder();
                String settingsString;
                while ((settingsString = br.readLine()) != null) {
                    formula.append(settingsString);
                }
                dataRobots.add(new DataRobot(formula.toString(), keyboardSimulator));
            }
        }

    }

    public String getNowAsString() {
        Calendar now = Calendar.getInstance();
        return String.valueOf(now.get(Calendar.YEAR)) + "-" + String.valueOf(now.get(Calendar.MONTH)) + "-" + String.valueOf(now.get(Calendar.DAY_OF_MONTH)) + "-" + String.valueOf(now.get(Calendar.HOUR_OF_DAY)) + "-" + String.valueOf(now.get(Calendar.MINUTE)) + "-" + String.valueOf(now.get(Calendar.SECOND));
    }

    public void destroy() {
        try {
            provider.reset();
            provider.stop();
            serialInputStream.close();
            serialOutputStream.close();
            serialPort.removeDataListener();
            serialPort.closePort();
        } catch (IOException ex) {
            Logger.getLogger(ScanHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public String getCurrentPath() {
        return System.getProperty("user.dir");
    }

    public boolean getConnectionStatus () {
        return this.foundDevice;
    }
}
