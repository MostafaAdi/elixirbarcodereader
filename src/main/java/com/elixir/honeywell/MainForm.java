package com.elixir.honeywell;

import com.elixir.honeywell.flatlaf.ElixirTheme;
import com.elixir.honeywell.utilities.ScanHandler;
import java.awt.AWTException;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.util.TooManyListenersException;
import javax.swing.JMenuItem;
import javax.swing.SwingUtilities;
import com.fazecast.jSerialComm.SerialPort;
import java.awt.ComponentOrientation;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.util.logging.Level;
import java.util.logging.Logger;
//import javax.swing.JFrame;
//import javax.swing.UIManager;
//import javax.swing.UnsupportedLookAndFeelException;

/**
 *
 * @author Ahmad Mujahid
 */
public class MainForm extends javax.swing.JFrame {

    private ScanHandler reader;
    static MainForm mainForm;
    public MainForm() {
        initComponents();
        initMeta();
    }
    private final String instructions = "يمكن قراءة باركود الهوية أو الطرود مباشرة"
            + "\n"
            + "دون الضغط على أي اختصار"
            + "\n"
            + "========================="
            + "\n"
            + "لالتقاط الصورة الأمامية للهوية"
            + "\n"
            + "ctrl + shift + f1"
            + "\n"
            + "========================="
            + "\n"
            + "لالتقاط الصورة الخلفية للهوية"
            + "\n"
            + "ctrl + shift + f2"
            + "\n"
            + "========================="
            + "\n"
            + "يجب أن يكون مؤشر الفأرة على حقل نوع"
            + "\n"
            + "الزبون عند قراءة باركود الهوية"
            + "\n"
            + "========================="
            + "\n"
            + "في حال فشل الاتصال بالجهاز تأكد من "
            + "\n"
            + "رقم المنفذ الخاص بالجهاز من"
            + "\n"
            + "إعدادات الأجهزة المتصلة"
            + "\n"
            + "الرقم الافتراضي هو"
            + "\n"
            + "COM4"
            + "\n"
            + "========================="
            + "\n"
            + "يمكن تحديد رقم المنفذ من ملف"
            + "\n"
            + "port.txt"
            + "\n"
            + "الموجود في نفس مجلد التطبيق"
            + "\n";

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        statusTextField = new javax.swing.JTextField();
        statusLabel = new javax.swing.JLabel();
        connectButton = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        jTextArea1.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
        menuBar = new javax.swing.JMenuBar();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 255));
        setForeground(java.awt.Color.white);
        setMinimumSize(new java.awt.Dimension(400, 400));
        setResizable(false);
        setSize(new java.awt.Dimension(400, 400));

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(0, 204, 0));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Elixir Intelligent Software");
        jLabel1.setAlignmentY(0.0F);

        statusTextField.setEditable(false);
        statusTextField.setFont(new java.awt.Font("Segoe UI", 0, 24)); // NOI18N
        statusTextField.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        statusTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                statusTextFieldActionPerformed(evt);
            }
        });

        statusLabel.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        statusLabel.setText("الحالة");

        connectButton.setFont(new java.awt.Font("Segoe UI", 0, 24)); // NOI18N

        jLabel2.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel2.setText("إرشادات");

        jTextArea1.setEditable(false);
        jTextArea1.setColumns(20);
        jTextArea1.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        jTextArea1.setRows(5);
        jTextArea1.setText(this.instructions);
        jTextArea1.setAutoscrolls(false);
        jTextArea1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jScrollPane1.setViewportView(jTextArea1);

        setJMenuBar(menuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(63, 63, 63)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 340, Short.MAX_VALUE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(connectButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(statusTextField)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(statusLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2))
                        .addGap(26, 26, 26))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 352, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(70, Short.MAX_VALUE))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(statusLabel)
                    .addComponent(statusTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(connectButton, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 287, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(100, Short.MAX_VALUE))
        );

        jScrollPane1.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);

        setBounds(0, 0, 498, 600);
    }// </editor-fold>//GEN-END:initComponents

    private void statusTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_statusTextFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_statusTextFieldActionPerformed

    public static void main(String args[]) {

        ElixirTheme.setup();       
//        JFrame.setDefaultLookAndFeelDecorated(true); //before creating JFrames
//        try {
//            UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
//        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
//            Logger.getLogger(MainForm.class.getName()).log(Level.SEVERE, null, ex);
//        }
        java.awt.EventQueue.invokeLater(() -> {
            mainForm = new MainForm();
            mainForm.setLocationRelativeTo(null);
            mainForm.setTitle("Elixir Barcode Reader");
            mainForm.setVisible(true);
            JMenuItem exitMenuItem = new JMenuItem("Exit");
            mainForm.connectButton.setText("غير متصل");
            mainForm.connectButton.setForeground(Color.RED);
            mainForm.connectButton.addActionListener((ActionEvent e) -> {
                mainForm.connectButtonAction();
            });
            exitMenuItem.addActionListener((ActionEvent e) -> {
                mainForm.dispose();
            });
            mainForm.connect();
            
        });

        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                if (mainForm.reader != null) {
                    mainForm.reader.destroy();
                }
            }
        });
    }

    public void connectButtonAction() {
        if (reader == null) {
            connect();
        } else {
            disconnect();
        }
    }

    public void connect() {
        SwingUtilities.invokeLater(() -> {
            mainForm.statusTextField.setText("جاري الاتصال...");
            mainForm.statusTextField.setForeground(Color.BLACK);
        });
        SerialPort[] ports = SerialPort.getCommPorts();
        switch (ports.length) {
            case 0:
                System.out.println("no device detected!, make sure the device is connected.");
                SwingUtilities.invokeLater(() -> {
                    mainForm.statusTextField.setText("فشل الاتصال");
                    mainForm.statusTextField.setForeground(Color.RED);
                    mainForm.connectButton.setText("غير متصل");
                    mainForm.connectButton.setForeground(Color.RED);
//                    mainForm.barcodeMode.setText("inactive");
//                    mainForm.barcodeMode.setForeground(inactiveColor);
                });
                break;
                
            default:
                try {
                    System.out.println("reading port from settings file...");
                    File settingsFile = new File( "port.txt");
                    FileInputStream is = new FileInputStream(settingsFile);
                    InputStreamReader isr = new InputStreamReader(is, "UTF-8");
                    BufferedReader br = new BufferedReader(isr);
                    StringBuilder port = new StringBuilder();
                    String portString;
                    while ((portString = br.readLine()) != null) {
                        port.append(portString);
                    }
                    System.out.println("port: " + port.toString().toUpperCase().trim());
                    reader = new ScanHandler(port.toString().toUpperCase().trim());
                    if (!reader.getConnectionStatus()) {
                        reader.destroy();
                        reader = null;
                        SwingUtilities.invokeLater(() -> {
                            mainForm.statusTextField.setText("فشل الاتصال");
                            mainForm.statusTextField.setForeground(Color.RED);
                            mainForm.connectButton.setText("إعادة المحاولة");
                            mainForm.connectButton.setForeground(Color.BLACK);
        //                    mainForm.barcodeMode.setText("inactive");
        //                    mainForm.barcodeMode.setForeground(inactiveColor);
                        });
                    } else {
                        SwingUtilities.invokeLater(() -> {
                            mainForm.statusTextField.setText("متصل");
                            mainForm.statusTextField.setForeground(Color.GREEN);
                            mainForm.connectButton.setText("متصل");
                            mainForm.connectButton.setForeground(Color.GREEN);
        //                    mainForm.barcodeMode.setText("inactive");
        //                    mainForm.barcodeMode.setForeground(inactiveColor);
                        });
                    }
                    
                } catch (AWTException | IOException | TooManyListenersException ex) {
                    
                }
                
        }
    }
    
//    public void updateMode(boolean barcodeMode) {
//        if (!barcodeMode) {
//            SwingUtilities.invokeLater(() -> {
//                mainForm.barcodeMode.setText("inactive");
//                mainForm.barcodeMode.setForeground(inactiveColor);
//            });
//            return;
//        }
//            
//        SwingUtilities.invokeLater(() -> {
//            mainForm.barcodeMode.setText("active");
//            mainForm.barcodeMode.setForeground(Color.GREEN);
//        });
//    }

    public void disconnect() {
        reader.destroy();
        reader = null;
        SwingUtilities.invokeLater(() -> {
            mainForm.statusTextField.setText("غير متصل");
            mainForm.statusTextField.setForeground(Color.RED);
            mainForm.connectButton.setText("غير متصل");
            mainForm.connectButton.setForeground(Color.RED);
        });
    }
    
    private void initMeta() {
        String idMeta = "String[] dataArray = dataText.split(\"#\");\n" +
            "boolean executed = false;\n" +
            "if(EUtility.containsNumber(dataArray[4]) && !EUtility.containsNumber(dataArray[0]))\n" +
            "{\n" +
            "	EUtility.pasteData(\"شخص\",1);\n" +
            "	EUtility.pasteData(dataArray[0].trim(),1);\n" +
            "	EUtility.pasteData(dataArray[2].trim(),1);\n" +
            "	EUtility.pasteData(dataArray[1].trim(),1);\n" +
            "	EUtility.pasteData(dataArray[3].trim(),1);\n" +
            "	EUtility.pasteData(\"هوية شخصية\",1);\n" +
            "	EUtility.pasteData(dataArray[5].trim(),1);\n" +
            "	EUtility.pasteData(\"سوريا\",1);\n" +
            "	EUtility.pasteData(EUtility.removeAllButArabic(dataArray[4]).trim(),1);\n" +
            "	EUtility.pasteData(EUtility.removeAllEnglish(EUtility.removeAllArabic(dataArray[4])).replace(\"-\",\"/\").trim(),1);\n" +
            "	executed = true;\n" +
            "}";
        String passportMeta = "String[] dataArray = dataText.split(\"#\");\n" +
            "boolean executed = false;\n" +
            "if(EUtility.containsNumber(dataArray[0]) && dataArray[0].contains(\"@\") && EUtility.containsNumber(dataArray[7]))\n" +
            "{\n" +
            "	EUtility.pasteData(\"شخص\",1);\n" +
            "	EUtility.pasteData(EUtility.removeAllButArabic(dataArray[0]).trim() , 1);\n" +
            "	EUtility.pasteData(dataArray[2].trim(),1);\n" +
            "	EUtility.pasteData(dataArray[1].trim(),1);\n" +
            "	EUtility.pasteData(dataArray[3].trim(),1);\n" +
            "	EUtility.pasteData(\"جواز سفر سوري\",1);\n" +
            "	EUtility.pasteData(dataArray[8].trim(),1);\n" +
            "	EUtility.pasteData(\"سوريا\",1);\n" +
            "	EUtility.pasteData(dataArray[4].replace(\"-\",\"/\").trim(),1);\n" +
            "	EUtility.pasteData(dataArray[5].trim(),1);\n" +
            "	executed = true;\n" +
            "}";
        
        File dir = new File("meta");
        dir.mkdir();
        createFile("meta", "id", "txt", idMeta);
        createFile("meta","passport", "txt", passportMeta);
        createFile("", "port", "txt", "COM4");
    }
    
    private void createFile(String dir, String filename, String ext, String content) {
        try {
            File file = new File(
                (dir.equalsIgnoreCase("")? "" : dir + "/") 
                + filename + "." + ext
            );
            if (file.exists() && !file.isDirectory()) return;
            Writer fw = new OutputStreamWriter(new FileOutputStream(file),  StandardCharsets.UTF_8);
            try (BufferedWriter bw = new BufferedWriter(fw)) {
                bw.write(content);
            }
        } catch (IOException ex) {
            Logger.getLogger(MainForm.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }
    


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton connectButton;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JLabel statusLabel;
    private javax.swing.JTextField statusTextField;
    // End of variables declaration//GEN-END:variables
}
