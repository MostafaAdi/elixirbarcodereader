package com.elixir.honeywell.flatlaf;

import com.formdev.flatlaf.FlatIntelliJLaf;

public class ElixirTheme
	extends FlatIntelliJLaf
{
	public static final String NAME = "ElixirTheme";

	public static boolean setup() {
		return setup( new ElixirTheme() );
	}

	public static void installLafInfo() {
		installLafInfo( NAME, ElixirTheme.class );
	}

	@Override
	public String getName() {
		return NAME;
	}
}
